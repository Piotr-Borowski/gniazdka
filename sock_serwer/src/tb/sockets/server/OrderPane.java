package tb.sockets.server;

import javax.swing.JPanel;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;

public class OrderPane extends JPanel {

	/**
	 * Create the panel.
	 */
	public OrderPane() {
		setBackground(new Color(255, 255, 240));
	}
		
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;

		if(MainFrame.shape != null)
		g2d.draw(MainFrame.shape);
	}
	

}

