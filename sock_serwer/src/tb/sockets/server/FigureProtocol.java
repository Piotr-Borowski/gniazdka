package tb.sockets.server;

import java.awt.Shape;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;
import java.util.Scanner;

public abstract class FigureProtocol {

	static String FigureToString(Shape s)
	{
		String str = "R/10/10/10/10";
		
		String shapeClass = s.getClass().getName();
		System.out.println(shapeClass);
	
		
		switch(shapeClass)
		{
			case "java.awt.geom.Rectangle2D$Double":
			case "java.awt.geom.Rectangle2D$Float":
			case "java.awt.geom.Rectangle2D":
				str = "R/";
				break;
				
			case "java.awt.geom.Ellipse2D$Double":
			case "java.awt.geom.Ellipse2D$Float":
			case "java.awt.geom.Ellipse2D":
				str = "E/";		
				break;
			case "java.awt.geom.Line2D$Double":
			case "java.awt.geom.Line2D$Float":
			case "java.awt.geom.Line2D":
				str = "L/";
				break;
		}
		
		str +=  s.getBounds().x + "/" + s.getBounds().y + "/" + s.getBounds().height + "/" + s.getBounds().width;
		return str;
		
	}
	
	static Shape StringToFigure(String str)
	{
		
		String[] tokens = str.split("/");
		Shape s = null;

		
		switch(tokens[0])
		{
			case "R": 
				s = new Rectangle2D.Double(Double.parseDouble(tokens[1]),Double.parseDouble(tokens[2]),Double.parseDouble(tokens[3]),Double.parseDouble(tokens[4]));
				break;
			case "E":
				s = new Ellipse2D.Double(Double.parseDouble(tokens[1]),Double.parseDouble(tokens[2]),Double.parseDouble(tokens[3]),Double.parseDouble(tokens[4]));
				break;
			case "L":
				s = new Line2D.Double(Double.parseDouble(tokens[1]),Double.parseDouble(tokens[2]),Double.parseDouble(tokens[3]),Double.parseDouble(tokens[4]));
				break;
		}
		
		return s;
		
	}
	
	static String DataToProtocolString(String figure, String x, String y, String height, String width)
	{
		String protocolString = "";
		
		if(figure.toLowerCase() == "rectangle")
			protocolString = "R/";
		else if(figure.toLowerCase() == "ellipse")
			protocolString = "E/";
		else if(figure.toLowerCase() == "line")
			protocolString = "L/";
		
		protocolString +=   x + "/" + y  + "/" + height + "/"+ width;
		
		return protocolString;
	}
}
