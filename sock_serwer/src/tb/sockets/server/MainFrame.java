package tb.sockets.server;

import java.awt.EventQueue;
import java.awt.Shape;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.text.FieldPosition;
import java.text.Format;
import java.text.ParseException;
import java.text.ParsePosition;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JFormattedTextField;
import java.awt.Color;

public class MainFrame extends JFrame {

	public static JPanel contentPane;
	static JLabel lblNotConnected = new JLabel("Not Connected");
	public static Shape shape;
	public static ArrayList<String> shapes = new ArrayList<String>();
	static Thread reader = new Thread(new ServerReader());
	static Thread writer = new Thread(new ServerWriter());
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainFrame frame = new MainFrame();
					frame.setVisible(true);
						
						
					
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		
		reader.start();
		writer.start();
	
	}



	/**
	 * Create the frame.
	 * @throws IOException 
	 */
	public MainFrame() throws IOException {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 650, 500);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		

		
		JButton btnAdd = new JButton("ADD");
		btnAdd.setBounds(10,200,75,23);
		contentPane.add(btnAdd);
		
		
		OrderPane panel = new OrderPane();
		panel.setBounds(145, 14, 487, 448);
		contentPane.add(panel);
		
		//lblNotConnected = new JLabel("Not Connected");
		lblNotConnected.setForeground(new Color(255, 255, 255));
		lblNotConnected.setBackground(new Color(128, 128, 128));
		lblNotConnected.setOpaque(true);
		lblNotConnected.setBounds(10, 104, 123, 23);
		contentPane.add(lblNotConnected);
		
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				shapes.add(FigureProtocol.FigureToString(shape));
			}
		});
			
		
	}
	

}

class ServerReader implements Runnable
{
	ServerReader()
	{}
	
	@Override
	public void run() {
		try {

		
		while(true)
		{
			ServerSocket sSock = new ServerSocket(6666);
			Socket sock = sSock.accept();
			
			
			
			DataInputStream in = new DataInputStream(sock.getInputStream());
			BufferedReader is = new BufferedReader(new InputStreamReader(in));
			
			
			synchronized(MainFrame.lblNotConnected)
			{
				MainFrame.lblNotConnected.setText("Connected");
				MainFrame.lblNotConnected.setBackground(new Color(0,200,0));
			}
		
			
			
			String text = is.readLine();
			//System.out.println("Przeczytano z gniazdka: " + text);
			is.close();
			
		
			sock.close();
			if(text == "DOWNLOAD")
			{
				System.out.println("Wysylam figury, czekam na polaczenie z portem 6688");
			}
			else {
				MainFrame.shape = FigureProtocol.StringToFigure(text);
				MainFrame.contentPane.repaint();
			}
			
			
			is.close();
			in.close();
			sock.close();
			sSock.close();
		}	
			
		} catch (IOException e) {
			synchronized(MainFrame.lblNotConnected)
			{
				MainFrame.lblNotConnected.setText("Not Connected");
				MainFrame.lblNotConnected.setBackground(new Color(200,0,0));
			}
			e.printStackTrace();
		}
		
	}

}


class ServerWriter implements Runnable
{
	ServerWriter()
	{}
	
	@Override
	public void run() {
		try {

	
		while(true)
		{
			ServerSocket sSock = new ServerSocket(6688);
			Socket sock = sSock.accept();
			
			
			
			DataInputStream in = new DataInputStream(sock.getInputStream());
			BufferedReader is = new BufferedReader(new InputStreamReader(in));
			
			
		
				
				System.out.println("AKCEPTUJE");
				
				
				DataOutputStream os = new DataOutputStream(sock.getOutputStream());
				//do testow
				os.writeBytes("R/10/10/10/10\n");
				for(String s : MainFrame.shapes)
				{
					os.writeBytes(s+"\n");
				}
				System.out.println("WYSLANO");
				os.close();
				sSock.close();
				
			
			
			
			is.close();
			in.close();
			sock.close();
			sSock.close();
		}	
			
		} catch (IOException e) {
			
			e.printStackTrace();
		}
		
	}

}

