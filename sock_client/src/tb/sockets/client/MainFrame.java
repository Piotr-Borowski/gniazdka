package tb.sockets.client;

import java.awt.EventQueue;
import java.awt.Shape;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.text.FieldPosition;
import java.text.Format;
import java.text.ParseException;
import java.text.ParsePosition;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.text.MaskFormatter;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JFormattedTextField;
import java.awt.Color;

public class MainFrame extends JFrame {

	private JPanel contentPane;
	private JFormattedTextField frmtdtxtfldIp;
	private Socket sock;
	static ArrayList<Shape> downloadedShapes = new ArrayList<Shape>();
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainFrame frame = new MainFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MainFrame() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 650, 500);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblHost = new JLabel("Host:");
		lblHost.setBounds(10, 14, 26, 14);
		contentPane.add(lblHost);
		
		JLabel lblX = new JLabel("X:");
		lblX.setBounds(10, 140, 20, 20);
		contentPane.add(lblX);
		JLabel lblY = new JLabel("Y:");
		lblY.setBounds(10, 170, 20, 20);
		contentPane.add(lblY);
		JTextField textFieldX = new JTextField("",3);
		textFieldX.setBounds(50, 140, 50, 20);
		contentPane.add(textFieldX);
		JTextField textFieldY = new JTextField("",3);
		textFieldY.setBounds(50, 170, 50, 20);
		contentPane.add(textFieldY);
		
		JLabel lblHeight = new JLabel("Height:");
		lblHeight.setBounds(10, 200, 40, 20);
		contentPane.add(lblHeight);
		JLabel lblWidth = new JLabel("Width:");
		lblWidth.setBounds(10, 230, 40, 20);
		contentPane.add(lblWidth);
		JTextField textFieldHeight = new JTextField("",3);
		textFieldHeight.setBounds(50, 200, 50, 20);
		contentPane.add(textFieldHeight);
		JTextField textFieldWidth = new JTextField("",3);
		textFieldWidth.setBounds(50, 230, 50, 20);
		contentPane.add(textFieldWidth);
		
		
		String[] figures = { "rectangle", "ellipse", "line"};
		JComboBox<String> combo = new JComboBox<String>(figures);
		combo.setBounds(50, 260 , 70, 20);
		contentPane.add(combo);
		
		
		
		
		
		//try {
			frmtdtxtfldIp = new JFormattedTextField(); //new MaskFormatter("###.###.###.###")
			frmtdtxtfldIp.setBounds(43, 11, 90, 20);
			frmtdtxtfldIp.setText("127.0.0.1");
			contentPane.add(frmtdtxtfldIp);
		//} catch (ParseException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		//}
		
		JButton btnConnect = new JButton("Connect");
		btnConnect.setBounds(10, 70, 65, 23);
		contentPane.add(btnConnect);
		
		JButton btnAdd = new JButton("Add SMTHNG");
		btnAdd.setBounds(10,300,75,23);
		contentPane.add(btnAdd);
		
		JButton btnDownload = new JButton("DOWNLOAD");
		btnDownload.setBounds(10,330,130,23);
		contentPane.add(btnDownload);
		
		
		JFormattedTextField frmtdtxtfldXxxx = new JFormattedTextField();
		frmtdtxtfldXxxx.setText("6666");
		frmtdtxtfldXxxx.setBounds(43, 39, 90, 20);
		contentPane.add(frmtdtxtfldXxxx);
		
		JLabel lblPort = new JLabel("Port:");
		lblPort.setBounds(10, 42, 26, 14);
		contentPane.add(lblPort);
		
		OrderPane panel = new OrderPane();
		panel.setBounds(145, 14, 487, 448);
		contentPane.add(panel);
		
		JLabel lblNotConnected = new JLabel("Not Connected");
		lblNotConnected.setForeground(new Color(255, 255, 255));
		lblNotConnected.setBackground(new Color(128, 128, 128));
		lblNotConnected.setOpaque(true);
		lblNotConnected.setBounds(10, 104, 123, 23);
		contentPane.add(lblNotConnected);
		
		btnConnect.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					System.out.println(frmtdtxtfldIp.getText());
					System.out.println(Integer.parseInt(frmtdtxtfldXxxx.getText()));
					sock = new Socket(frmtdtxtfldIp.getText(), Integer.parseInt(frmtdtxtfldXxxx.getText()));
					DataOutputStream so = new DataOutputStream(sock.getOutputStream());
					lblNotConnected.setText("Connected");
					lblNotConnected.setBackground(new Color(0,200,0));
					so.writeChars("Polaczono");
					so.close();
					sock.close();
				} catch (IOException e1) {
					e1.printStackTrace();
					lblNotConnected.setText("Not Connected");
					lblNotConnected.setBackground(new Color(200,0,0));
				}
			}
		});
		
		
		
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					
					sock = new Socket(frmtdtxtfldIp.getText(), Integer.parseInt(frmtdtxtfldXxxx.getText()));
					DataOutputStream so = new DataOutputStream(sock.getOutputStream());
					lblNotConnected.setText("Connected");
					lblNotConnected.setBackground(new Color(0,200,0));
					
					so.writeBytes(FigureProtocol.DataToProtocolString(combo.getSelectedItem().toString(),
							textFieldX.getText() , 
							textFieldY.getText(), 
							textFieldHeight.getText(), 
							textFieldWidth.getText()));
					
					
					so.close();
					sock.close();
				} catch (IOException e1) {
					e1.printStackTrace();
					lblNotConnected.setText("Not Connected");
					lblNotConnected.setBackground(new Color(200,0,0));
				}
			}
		});
		
		
		btnDownload.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					
					sock = new Socket(frmtdtxtfldIp.getText(), Integer.parseInt(frmtdtxtfldXxxx.getText()));
					DataOutputStream so = new DataOutputStream(sock.getOutputStream());
				
					
					so.writeBytes("DOWNLOAD");
					so.close();
					sock.close();
					
					lblNotConnected.setText("Connected");
					lblNotConnected.setBackground(new Color(0,200,0));
					
					
					
					Socket sock1 = new Socket(frmtdtxtfldIp.getText(), 6688);
					
					DataInputStream in = new DataInputStream(sock1.getInputStream());
					BufferedReader is = new BufferedReader(new InputStreamReader(in));
					String line;
					
					while((line = is.readLine()) != null)
					{
						System.out.println("POBIERAM");
						System.out.println(line);
						downloadedShapes.add(FigureProtocol.StringToFigure(line));
					}
					
					
					
					
					
					
					in.close();
					is.close();
					sock1.close();
					contentPane.repaint();
				} catch (IOException  e1) {
					e1.printStackTrace();
					lblNotConnected.setText("Not Connected");
					lblNotConnected.setBackground(new Color(200,0,0));
				}
			}
		});
		
		
	}
	

}
